<?php

declare(strict_types = 1);

namespace Drupal\entity_ref_filtering_link\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_filtered_link_disable",
 *   label = @Translation("Filter Link Disable"),
 *   description = @Translation("Display the label of the referenced entities linking to a view to filter."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   no_ui = true
 * )
 */
class EntityReferenceFilteredLinkDisableFormatter extends EntityReferenceFilteredLinkFormatter {
}
