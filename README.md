ABOUT
-----

Entity Reference Filtering Link provides a new configurable field formatter
for entity reference fields. The filter lets you use tags on a node (or other
entity) to serve as links to a pre-filtered search page (or any view).

Per field instance, the path, mode and argument name can be specified. For
clarity with a base page of `/search`, argument name of `tag`, and a taxonomy
term "StarTrek" with id of 7 the following would be the resultant urls:

| Mode                                          | Example URL                                  |
|-----------------------------------------------|----------------------------------------------|
| Standard [$id]                                | https://example.com/search?tag=7             |
| Standard Multiple[] [$id]                     | https://example.com/search?tag[]=7           |
| Standard Double Bracketed Multiple[$id] [$id] | https://example.com/search?tag[7]=7          |
| Autocomplete [$label ($id)]                   | https://example.com/search?tag=StarTrek (7)  |
| Facet - ID [$id]                              | https://example.com/search?f[0]=tag:7        |
| Facet - Label [$label]                        | https://example.com/search?f[0]=tag:StarTrek |
| Label [$label]                                | https://example.com/search?tag=StarTrek      |

A second field formatter is provided with identical functionality other than an
added option to exclude some entities from the link by label.

REQUIREMENTS
------------

Drupal 9 or 10. Field UI is strongly recommended for configuration.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See the
[install docs](https://www.drupal.org/docs/extending-drupal/installing-modules)
if required in the Drupal documentation for further information.

CONFIGURATION
-------------

Configuration can be set as normal for field formatters through the Field UI.
For example, for an Entity Reference field on the Node type `Article`, that
would be on `/admin/structure/types/manage/article/form-display`.

CREDITS
-------

Primarily developed with support from [Taoti Creative](https://taoti.com).

Special thanks to Charles (Cleverington), Lucas (lucasgrecco) &
Santiago (nexonOSX) for their early feedback and testing.

FAQ
---

Any questions? Ask away on the issue queue or contact the maintainer.
